// stdlib functionality
#include <iostream>
// ROOT functionality
#include <TFile.h>
#include <TH1D.h>
#include <TLorentzVector.h>
// ATLAS EDM functionality
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonContainer.h"

#include <xAODRootAccess/tools/TReturnCode.h>

int main() {

  // initialize the xAOD EDM
  xAOD::TReturnCode::enableFailure();
  R__CHECK (xAOD::Init() );
  // open the input file
  TString inputFilePath = "/home/atlas/Bootcamp/Data/DAOD_EXOT27.17882744._000026.pool.root.1";
  xAOD::TEvent event;
  std::unique_ptr< TFile > iFile ( TFile::Open(inputFilePath, "READ") );
  std::cout << "here" <<std::endl;
  R__CHECK ( event.readFrom( iFile.get() ) );
  std::cout << "here" <<std::endl;

  // define histograms

  TH1D *h_njets_raw = new TH1D("h_njets_raw","",20,0,20);
  TH1D *h_njets_pt_eta = new TH1D("h_njets_pt_eta","",20,0,20);
  TH1D *h_mjj_raw = new TH1D("h_mjj_raw","",100,0,500);
  TH1D *h_mjj_pt_eta = new TH1D("h_mjj_pt_eta","",100,0,500);
  std::cout << "here" <<std::endl;

  // for counting events
  unsigned count = 0;
  std::cout << "here" <<std::endl;

  // get the number of events in the file to loop over
  const Long64_t numEntries = event.getEntries();
  std::cout << "Number of Events: " <<numEntries << std::endl;

  // primary event loop
  for ( Long64_t i=0; i<numEntries; ++i ) {

    // Load the event
    event.getEntry( i );

    // Load xAOD::EventInfo and print the event info
    const xAOD::EventInfo * ei = nullptr;
    R__CHECK   ( event.retrieve( ei, "EventInfo" ) );
    //std::cout << "Processing run # " << ei->runNumber() << ", event # " << ei->eventNumber() << std::endl;

    // retrieve the jet container from the event store
    const xAOD::JetContainer* jets = nullptr;
    R__CHECK (event.retrieve(jets, "AntiKt4EMTopoJets") ) ;

    //const xAOD::ElectronContainer* electrons = nullptr;
    //R__CHECK (event.retrieve(electrons, "Electrons") ) ;

    const xAOD::MuonContainer* muons = nullptr;
    R__CHECK (event.retrieve(muons, "Muons") ) ;

    //vector to store muons in the event
    std::vector<xAOD::Muon> selectedMuons;

    for (const xAOD::Muon* muon : *muons){
   
        selectedMuons.push_back(*muon);

    }


    // vector to store jets in this event
    std::vector<xAOD::Jet> jets_raw;
    std::vector<xAOD::Jet> jets_pt_eta;

    std::vector<xAOD::Jet> e_raw;
    std::vector<xAOD::Jet> mu_raw;

    // loop through all of the jets and make selections with the helper
    for(const xAOD::Jet* jet : *jets) {
      // print the kinematics of each jet in the event
      // std::cout << "Jet : pt=" << jet->pt() << "  eta=" << jet->eta() << "  phi=" << jet->phi() << "  m=" << jet->m() << std::endl;

      jets_raw.push_back(*jet);

      if(jet->pt()/1000. > 50 && std::abs(jet->eta()<2.5) ){
          //count the number of jets
          jets_pt_eta.push_back(*jet);
       }
    }


    std::cout << "Number of Muons: " << selectedMuons.size() << std::endl;
    h_njets_raw->Fill( jets_raw.size());
    h_njets_pt_eta->Fill( jets_pt_eta.size());

    if(jets_raw.size() >=2 ){
      h_mjj_raw->Fill( (jets_raw.at(0).p4() + jets_raw.at(1).p4()).M()/1000. );
    }
 
    if(jets_pt_eta.size() >=2 ){
      h_mjj_pt_eta->Fill( (jets_raw.at(0).p4() + jets_raw.at(1).p4()).M()/1000. );
    }



    // counter for the number of events analyzed thus far
    count += 1;
  }

  //output file definition

  TFile *fout = new TFile("myOutputFile.root","RECREATE"); 

  h_njets_raw->Write();
  h_njets_pt_eta->Write();
  h_mjj_raw->Write();
  h_mjj_pt_eta->Write();
  fout->Close();

  // exit from the main function cleanly
  return 0;
}


